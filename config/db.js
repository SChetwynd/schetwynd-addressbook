// creating a database instance, this file will be imported by every module
// that requires access to the database

var Seq = require('sequelize');

const sequelize = new Seq('mydb', 'stiv', '218876951', {
  dialect: 'mysql',
  host: 'localhost',
  port: 3306
});

sequelize.authenticate().then(function() {
  console.log('Successful connection to database.');
}).catch(function(err) {
  console.log('Connection to the database failed: ', err);
});

module.exports = sequelize;
