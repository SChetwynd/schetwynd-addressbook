var Person = require('../models/person');
var Address = require('../models/address');
var Telephone = require('../models/telephone');
var Email = require('../models/email');
var async = require('async');

const { body,validationResult } = require('express-validator/check');
const { sanitizeBody } = require('express-validator/filter');

// Display list of all people.
exports.people_list = function(req, res) {
  Person.findAll({
    attributes: ['first_name', 'last_name']
  }).then(function(results) {
    res.render('people', {title: 'People in your address book', people_list: results});
  });
}

// Displayes an empty form upon request
exports.new_person_get = function(req, res){

  async.parallel({
    telephonetypes: function(callback){
      Telephone.telephonetype.findAll().then(function(result) {
        callback(null, result);
      });
    },
    emailtypes: function(callback){
      Email.emailtype.findAll().then(function(result) {
        callback(null, result);
      });
    },
  }, function(err, results) {
      res.render('newperson', {title: 'Enter the details of a new person', telephonetype: results.telephonetypes, emailtype: results.emailtypes});
  });
}

// handles post request containing data for a new person
exports.new_person_post = [

  // Validating that the fields are the correct length
  body('first_name', 'A first name is required').isLength({ min: 1}).trim(),
  body('last_name', 'A last name is required').isLength({ min: 1}).trim(),
  body('date_of_birth', 'Invalid Date of Birth').optional({checkFalsy: true}).isISO8601(),

  // TODO: test if an address has been input then verify it, or not
  body('postcode', 'That is not a valid UK postcode').optional().isPostalCode('GB').trim(),

  // TODO: allow multiple inputs for same Type
  body('email', 'That is not a valid email address').optional().isEmail(),

  // Sanitizing fields to remove any potentionally harmful entries
  sanitizeBody('first_name').trim().escape(),
  sanitizeBody('middle_name').trim().escape(),
  sanitizeBody('last_name').trim().escape(),
  sanitizeBody('date_of_birth').toDate(),
  sanitizeBody('number_name').trim().escape(),
  sanitizeBody('road').trim().escape(),
  sanitizeBody('locality').trim().escape(),
  sanitizeBody('town').trim().escape(),
  sanitizeBody('postcode').trim().escape(),
  sanitizeBody('email').trim().escape().normalizeEmail(),
  sanitizeBody('emailtype').trim().escape(),
  sanitizeBody('telephone').trim().escape(),
  sanitizeBody('telephonetype').trim().escape(),

  function(req, res, next) {
    // Are there any validation errors?
    const errors = validationResult(req);

    var new_person = new Person(
      {
        first_name: req.body.first_name,
        middle_name: req.body.middle_name,
        last_name: req.body.last_name,
        date_of_birth: req.body.date_of_birth,
      }
    );

    var new_address = new Address(
      {
        number_name: req.body.number_name,
        road: req.body.road,
        locality: req.body.locality,
        town: req.body.town,
        postcode: req.body.postcode,
      }
    )

    // if there are errors, the form gets passed back the the user with the error messages
    if (!errors.isEmpty()) {
      res.render('newperson', {title: 'Enter the details of a new person', person: new_person, address: new_address, errors: errors.array()});
      return;
    } else {
      var address = null; // holds address id to be associated with the person
      // if the number_name is empty assume there is no address input
      // TODO: test to ensure above is correct
      if (new_address.road != null) {
        Address.findOrCreate({where: {
          number_name: new_address.number_name,
          road: new_address.road,
          locality: new_address.locality,
          town: new_address.town,
          postcode: new_address.postcode
        }}).then(function(result){
          address = result[0];
        });
      }
      // The form is valid
      // find or create the person, then redirect to the persons page
      Person.findOrCreate({where: {
        first_name: new_person.first_name,
        middle_name: new_person.middle_name,
        last_name: new_person.last_name,
        date_of_birth: new_person.date_of_birth
      }}).then(function (result) {
        if (address != null) {
          console.log('Address id: ' + address.id);
          address.addPerson(result[0]);
        }
        res.redirect('/person/' + result[0].id);
      });
    }
  }
];

exports.person_get = function(req, res, next) {
  res.send('Not yet implemented: specific person');
}

exports.person_update_get = function(req, res, next) {
  res.send('Not yetr Implemented: person update get');
}

exports.person_update_post = function(req, res, next) {
  res.send('Not yet implemented: person update post');
}
