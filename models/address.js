var sequelize = require('sequelize');
var Person = require('./person');
var db = require('../config/db');


var addressSchema = db.define('Address',
  {
    id: {
      type: sequelize.INTEGER,
      primaryKey: true,
      autoIncrement: true
    },
    number_name: {
      type: sequelize.STRING(45),
      allowNull: false,
    },
    road: sequelize.STRING(45),
    locality: sequelize.STRING(45),
    town: {
      type: sequelize.STRING(45),
      allowNull: false,
    },
    postcode: {
      type: sequelize.STRING(8),
      allowNull: false,
    }

});

addressSchema.hasMany(Person);

db.sync({force: false});

module.exports = addressSchema;
