var sequelize = require('sequelize');
var person = require('./person');
var db = require('../config/db');

var telephoneSchema = db.define('Telephone',
  {
    id: {
      type: sequelize.INTEGER,
      primaryKey: true,
      autoIncrement: true,
    },
    number: {
      type: sequelize.INTEGER,
      allowNull: false,
    }
});

var telephoneTypeSchema = db.define('TelephoneType',
  {
    id: {
      type: sequelize.INTEGER,
      primaryKey: true,
      autoIncrement: true,
    },
    telephonetype: sequelize.STRING(45),
});

telephoneTypeSchema.hasMany(telephoneSchema);

// TODO: remove anything which creates or tables. or default values
telephoneTypeSchema.findOrCreate({where: {telephonetype: 'home'}});
telephoneTypeSchema.findOrCreate({where: {telephonetype: 'mobile'}});
telephoneTypeSchema.findOrCreate({where: {telephonetype: 'work'}});
telephoneSchema.sync({force: false});
telephoneTypeSchema.sync({force: false});

exports.telephone = telephoneSchema;
exports.telephonetype = telephoneTypeSchema;
