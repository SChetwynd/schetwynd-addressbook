var sequelize = require('sequelize');
var person = require('./person');
var db = require('../config/db');

var emailSchema = db.define('Email',
  {
    id: {
      type: sequelize.INTEGER,
      primaryKey: true,
      autoIncrement: true,
    },
    address: {
      type: sequelize.STRING(100),
      allowNull: false,
    }
});

var emailTypeSchema = db.define('EmailType',
  {
    id: {
      type: sequelize.INTEGER,
      primaryKey: true,
      autoIncrement: true,
    },
    emailtype: sequelize.STRING(45),
});

emailTypeSchema.hasMany(emailSchema);

// TODO: Delete thing which create and put records into database
emailSchema.sync({force: false});
emailTypeSchema.sync({force: false});
emailTypeSchema.findOrCreate({where: {emailtype: 'home'}});
emailTypeSchema.findOrCreate({where: {emailtype: 'work'}});
emailTypeSchema.findOrCreate({where: {emailtype: 'personal'}});

exports.email = emailSchema;
exports.emailtype = emailTypeSchema;
