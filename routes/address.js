var express = require('express');
var router = express.Router();
var person_controller = require('../controllers/personController');


// GET request to update an address, navigating to page only, no info transmitted
// in get
router.get('/person/:id/update', person_controller.person_update_get);

// POST request for an address to be updated, info contained in POST
router.post('/person/:id/update', person_controller.person_update_post);

// GET request for a specific person
router.get('/person/:id', person_controller.person_get)

// GET request for all people
router.get('/people', person_controller.people_list)

// GET request for a new person, simply to display the page, no data is transmitted
// in GET
router.get('/new_person', person_controller.new_person_get)

// POST request for a new person, Data for the new person request is contained
// in POST
router.post('/new_person', person_controller.new_person_post);

module.exports = router;
