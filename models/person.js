var sequelize = require('sequelize');
var Email = require('./email');
var Telephone = require('./telephone');
var db = require('../config/db');

var personSchema = db.define('People',
  {
    id: {
      type: sequelize.INTEGER,
      primaryKey: true,
      autoIncrement: true,
    },
    first_name: sequelize.STRING(45),
    middle_name: sequelize.STRING(45),
    last_name: sequelize.STRING(45),
    date_of_birth: sequelize.DATE,
});

//personSchema.sync({force: false});

personSchema.hasMany(Email.email);
personSchema.hasMany(Telephone.telephone);

module.exports = personSchema;
